package com.passwordmanager.exceptions;

/**
 * Created by bdrosatos on 27/11/2017.
 */
public class ClientCertificateExpiredException extends Exception {
    public ClientCertificateExpiredException(String message) {
        super(message);
    }


}
