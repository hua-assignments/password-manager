package com.passwordmanager.exceptions;

/**
 * Created by bdrosatos on 27/11/2017.
 */
public class ClientCertificateNotValidException extends Exception {
    public ClientCertificateNotValidException(String message) {
        super(message);
    }
}
