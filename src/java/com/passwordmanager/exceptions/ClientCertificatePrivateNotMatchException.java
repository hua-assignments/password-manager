package com.passwordmanager.exceptions;

/**
 * Created by bdrosatos on 27/11/2017.
 */
public class ClientCertificatePrivateNotMatchException extends Exception {
    public ClientCertificatePrivateNotMatchException(String message) {
        super(message);
    }
}
