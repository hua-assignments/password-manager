package com.passwordmanager.exceptions;

public class DomainPasswordUnauthorizedChangeException extends Exception {
    public DomainPasswordUnauthorizedChangeException(String message) {
        super(message);
    }
}
