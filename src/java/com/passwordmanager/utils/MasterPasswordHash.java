package com.passwordmanager.utils;

import com.passwordmanager.models.User;
import org.bouncycastle.util.encoders.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

//https://gist.github.com/jtan189/3804290
public class MasterPasswordHash {

    private User user;
    private String password;
    private String username;
    private String sKey;
    private String authHash;


    //gives sKey
    public String generateSKey() throws InvalidKeySpecException, NoSuchAlgorithmException {
        this.password = user.getPassword();
        this.username = user.getUsername();

        byte[] hash = SecurityUtils.pbkdf2(this.password.toCharArray(), this.username.getBytes(),
                2000, 16);

        this.sKey = new String(Base64.encode(hash));


        return this.sKey;
    }

    public String generateAuthHash() throws InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] hash = SecurityUtils.pbkdf2(this.sKey.toCharArray(), this.password.getBytes(),
                1000, 16);

        this.authHash = new String(Base64.encode(hash));

        return this.authHash;
    }

    public void setUser(User user) {
        this.user = user;
    }
}