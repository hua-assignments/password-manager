package com.passwordmanager.utils;

import com.passwordmanager.Main;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import sun.misc.BASE64Encoder;
import sun.security.provider.X509Factory;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLDecoder;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

public class Utils {
    public static String getProgramPath() throws UnsupportedEncodingException {
        URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
        String jarPath = URLDecoder.decode(url.getFile(), "UTF-8");
        return new File(jarPath).getParentFile().getPath();
    }

    public static void saveCertificatePrivateToUserFolder(Certificate certificate, PrivateKey key, String username) throws IOException,
            CertificateEncodingException {
        String newDir = Utils.getProgramPath() + System.getProperty("file.separator") + username
                + System.getProperty("file.separator");
        File file = new File(newDir);
        file.mkdir();
        File certificateFile = new File(file.getAbsolutePath() + "\\certificate.csr");
        PrintStream out = new PrintStream(certificateFile);

        // Write client certificate to file
        BASE64Encoder encoder = new BASE64Encoder();
        out.println(X509Factory.BEGIN_CERT);
        encoder.encodeBuffer(certificate.getEncoded(), out);
        out.println(X509Factory.END_CERT);
        out.close();

        // Write private key to file
        File privateKeyFile = new File(file.getAbsolutePath() + "\\privateKey.pem");
        PemWriter pemWriter = new PemWriter(new OutputStreamWriter(new FileOutputStream(privateKeyFile)));
        pemWriter.writeObject(new PemObject("RSA PRIVATE KEY", key.getEncoded()));
        pemWriter.close();

    }

    public static X509Certificate loadCertificateFromFile(File file) throws CertificateException, IOException {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        FileInputStream is = new FileInputStream(file);
        X509Certificate certificate = (X509Certificate) fact.generateCertificate(is);
        is.close();
        return certificate;
    }

    //https://stackoverflow.com/questions/11787571/how-to-read-pem-file-to-get-private-and-public-key
    public static PrivateKey loadPrivateKeyFromFile(File file) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        InputStream is = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        boolean inKey = false;
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            if (!inKey) {
                if (line.startsWith("-----BEGIN ") &&
                        line.endsWith(" PRIVATE KEY-----")) {
                    inKey = true;
                }
            } else {
                if (line.startsWith("-----END ") &&
                        line.endsWith(" PRIVATE KEY-----")) {
                    inKey = false;
                    break;
                }
                builder.append(line);
            }
        }
        //
        byte[] encoded = DatatypeConverter.parseBase64Binary(builder.toString());
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey key = kf.generatePrivate(keySpec);
        is.close();

        return key;
    }

    public static KeyStore loadKeystore(File file, char[] keystorePasswordCharArray) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore ks = KeyStore.getInstance("PKCS12");
        InputStream readStream = new FileInputStream(file);
        ks.load(readStream, keystorePasswordCharArray);
        readStream.close();

        return ks;
    }
}
