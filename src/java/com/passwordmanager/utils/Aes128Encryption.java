package com.passwordmanager.utils;


import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Arrays;

public class Aes128Encryption {

    private String initVector;

    public Aes128Encryption() throws UnsupportedEncodingException {
        byte[] bytes = {0, 1, 0, -2, 4, - 1, 2, -4, 0, 3, -2, 1, 2, 10, 23, -30};
        this.initVector = new String(Base64.encode(bytes));
    }

    public String encrypt(String plainPassword, String skey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        SecretKeySpec secretKeySpec = new SecretKeySpec(Base64.decode(skey), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", new BouncyCastleProvider());
        IvParameterSpec iv = new IvParameterSpec(Base64.decode(this.initVector));
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, iv);
        byte[] encrypted = cipher.doFinal(plainPassword.getBytes());
        return new String(Base64.encode(encrypted));
    }

    public String decrypt(String cipherText, String skey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        SecretKeySpec secretKeySpec = new SecretKeySpec(Base64.decode(skey), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", new BouncyCastleProvider());
        IvParameterSpec iv = new IvParameterSpec(Base64.decode(this.initVector));

        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);
        return new String(cipher.doFinal(Base64.decode(cipherText.getBytes())));
    }


}
