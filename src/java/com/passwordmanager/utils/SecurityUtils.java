package com.passwordmanager.utils;


import com.passwordmanager.Main;
import com.passwordmanager.exceptions.ClientCertificateExpiredException;
import com.passwordmanager.exceptions.ClientCertificateNotValidException;
import com.passwordmanager.exceptions.ClientCertificatePrivateNotMatchException;
import com.passwordmanager.exceptions.DomainPasswordUnauthorizedChangeException;
import com.passwordmanager.models.Domain;
import com.passwordmanager.models.User;
import com.passwordmanager.services.DomainsService;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;


import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.security.cert.Certificate;

import static org.bouncycastle.pqc.math.linearalgebra.ByteUtils.toHexString;

public class SecurityUtils {

    public final static String MID_SECURITY_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{6,20}";  // >=6, 1 digit
    public final static String HIGH_SECURITY_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,20}"; // >=8, 1 upper, 1 digit, 1 special char

    //https://stackoverflow.com/questions/29852290/self-signed-x509-certificate-with-bouncy-castle-in-java: Using Bouncycastle latest version - 1.55
    public static X509Certificate createCaCertificate(KeyPair keyPair, String subjectDN) throws OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException {
        Provider bcProvider = new BouncyCastleProvider();
        Security.addProvider(bcProvider);

        long now = System.currentTimeMillis();
        Date startDate = new Date(now); // The start date of the certificate, set now

        X500Name dnName = new X500Name(subjectDN);
        BigInteger certSerialNumber = new BigInteger(Long.toString(now)); // Create serial number for the certificate as the now timestamp

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 1); // Set ca certification expiration in one year from now

        Date expireDate = calendar.getTime();

        String signatureAlgorithm = "SHA256WithRSA"; // The algorithm to be used for the signature

        ContentSigner contentSigner = new JcaContentSignerBuilder(signatureAlgorithm).build(keyPair.getPrivate());

        JcaX509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(dnName,
                certSerialNumber, startDate, expireDate, dnName, keyPair.getPublic());

        certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(true)) // Allowed to sign other certificates
                .addExtension(Extension.keyUsage, true, // Add key usages
                        new KeyUsage(KeyUsage.digitalSignature
                                | KeyUsage.keyCertSign
                                | KeyUsage.cRLSign));


        return new JcaX509CertificateConverter().setProvider(bcProvider).getCertificate(certBuilder.build(contentSigner));
    }

    //https://stackoverflow.com/questions/18633273/correctly-creating-a-new-certificate-with-an-intermediate-certificate-using-boun
    //https://www.cryptoworkshop.com/guide/cwguide-070313.pdf
    public static X509Certificate createClientCertificate(KeyPair keyPair, KeyStore keyStore, String subjectDN) throws KeyStoreException, IOException, CertificateException,
            UnrecoverableKeyException, NoSuchAlgorithmException, OperatorCreationException {
        Provider bcProvider = new BouncyCastleProvider();
        Security.addProvider(bcProvider);

        long now = System.currentTimeMillis();
        Date startDate = new Date(now); // The start date of the certificate, set now
        BigInteger certSerialNumber = new BigInteger(Long.toString(now)); // Create serial number for the certificate as the now timestamp

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.MONTH, 6); // Set client certification expiration in 6 months from now

        Date expireDate = calendar.getTime();

        X509Certificate caCertificate = (X509Certificate) keyStore.getCertificate("caCertificate");
        PrivateKey caPrivate = (PrivateKey) keyStore.getKey("keyPrivate", Main.KEYSTORE_PASSWORD);

        String signatureAlgorithm = "SHA256WithRSA"; // The algorithm to be used for the signature

        JcaX509v3CertificateBuilder certBuilder =
                new JcaX509v3CertificateBuilder(
                        caCertificate.getSubjectX500Principal(),
                        certSerialNumber,
                        startDate,
                        expireDate,
                        new X500Principal(subjectDN),
                        keyPair.getPublic());

        JcaX509ExtensionUtils extUtils = new JcaX509ExtensionUtils();
        certBuilder.addExtension(Extension.authorityKeyIdentifier,
                false, extUtils.createAuthorityKeyIdentifier(caCertificate)) // Add the ca certificate of application as  Authority Key identifier
                .addExtension(Extension.subjectKeyIdentifier,
                        false, extUtils.createSubjectKeyIdentifier(keyPair.getPublic())) // Identifier for the public key
                .addExtension(Extension.basicConstraints,
                        true, new BasicConstraints(false)) // Do not allow to sign other certificates
                .addExtension(Extension.keyUsage, // Add key usages
                        true, new KeyUsage(KeyUsage.digitalSignature
                                | KeyUsage.keyEncipherment));

        ContentSigner signer = new JcaContentSignerBuilder(signatureAlgorithm).setProvider(bcProvider).build(caPrivate);


        return new JcaX509CertificateConverter().setProvider(bcProvider).getCertificate(certBuilder.build(signer));
    }

    //https://stackoverflow.com/questions/23265698/validate-if-rsa-key-matches-x-509-certificate-in-java
    public static void validateCertificateWithPrivateKey(X509Certificate certificate, PrivateKey privateKey)
            throws NoSuchAlgorithmException, ClientCertificatePrivateNotMatchException {

        final PublicKey publicKey = certificate.getPublicKey();
        final RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
        final byte[] certModulusData = rsaPublicKey.getModulus().toByteArray();

        final MessageDigest sha256 = MessageDigest.getInstance("SHA256");
        final byte[] certID = sha256.digest(certModulusData);
        final String certIDinHex = toHexString(certID);

        final RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
        final byte[] keyModulusData = rsaPrivateKey.getModulus().toByteArray();
        final byte[] keyID = sha256.digest(keyModulusData);
        final String keyIDinHex = toHexString(keyID);

        if (!certIDinHex.equalsIgnoreCase(keyIDinHex)) {
            throw new ClientCertificatePrivateNotMatchException("The certificate does not match with the private key provided");
        }
    }

    public static void validateClientCertificate(KeyStore keyStore, X509Certificate clientCertificate)
            throws ClientCertificateExpiredException, ClientCertificateNotValidException {

        try {
            Certificate caCertificate = keyStore.getCertificate("caCertificate");

            if (!caCertificate.equals(clientCertificate)) { // NOT CA certificate
                //check if is signed by ca
                clientCertificate.verify(caCertificate.getPublicKey()); // Throws exception if verify is unsuccessful
            }
        } catch (Exception e) {
            e.printStackTrace();
            // If exception occur throw ClientCertificateNotValidException
            throw new ClientCertificateNotValidException("Client certificate is not issued from this authority.");
        }

        // If it is the CA certificate OR signed by CA check if its expired
        try {
            clientCertificate.checkValidity(new Date()); // Throws exception if it's not valid.
        } catch (CertificateExpiredException e) {
            // If its expired throw ClientCertificateExpiredException
            throw new ClientCertificateExpiredException("Client Certificate Expired");
        } catch (CertificateNotYetValidException e) {
            throw new ClientCertificateExpiredException("Client Certificate Not valid yet");
        }

    }


    public static KeyStore generateKeystore(KeyPair keyPair, Certificate caCertificate) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {

        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(null, (Long.toString(System.currentTimeMillis())).toCharArray());

        // Save private key and certificate to keystore.
        ks.setKeyEntry("keyPrivate", keyPair.getPrivate(), Main.KEYSTORE_PASSWORD, new java.security.cert.Certificate[]{caCertificate});
        ks.setCertificateEntry("caCertificate", caCertificate);

        return ks;
    }

    public static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return skf.generateSecret(spec).getEncoded();
    }


    // Digital sign current user encrypted domains at app close
    //http://www.java2s.com/Code/Java/Security/SignatureSignAndVerify.htm
    public static void digitalSignUserDomains(User currentUser, KeyStore keyStore) throws NoSuchAlgorithmException, NoSuchProviderException,
            SignatureException, UnrecoverableKeyException, KeyStoreException, InvalidKeyException,
            UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException,
            InvalidAlgorithmParameterException, NoSuchPaddingException {

        DomainsService domainsService = new DomainsService(Main.entityManagerFactory, Main.entityManager);

        Domain criteria = new Domain();
        criteria.user = currentUser;
        List<Domain> currentUserDomains = domainsService.getList(criteria);

        for (Domain domain : currentUserDomains) {
            String hashedPassword = domain.getHashedPassword();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            byte[] hashedPasswordDigest = messageDigest.digest(hashedPassword.getBytes());
            String pair = domain.getDomainUrl() + new String(hashedPasswordDigest);

            Signature signer = Signature.getInstance("SHA1WithRSA", new BouncyCastleProvider());
            signer.initSign((PrivateKey) keyStore.getKey("keyPrivate", Main.KEYSTORE_PASSWORD));
            signer.update(pair.getBytes());

            byte[] sign = signer.sign();

            Aes128Encryption aes128Encryption = new Aes128Encryption();

            String encryptedSignature = aes128Encryption.encrypt(new String(sign), currentUser.getSkey());

            domain.setEncryptedSignature(encryptedSignature);

            domain.setUser(currentUser);
            domainsService.update(domain);
        }

    }

    //Verify the signature for current user encrypted domains at user login
    public static void verifySignUserDomain(User currentUser, KeyStore keyStore) throws DomainPasswordUnauthorizedChangeException,
            NoSuchAlgorithmException, NoSuchProviderException,
    SignatureException, UnrecoverableKeyException, KeyStoreException, InvalidKeyException,
    UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException,
    InvalidAlgorithmParameterException, NoSuchPaddingException {
        DomainsService domainsService = new DomainsService(Main.entityManagerFactory, Main.entityManager);

        Domain criteria = new Domain();
        criteria.user = currentUser;
        List<Domain> currentUserDomains = domainsService.getList(criteria);

        List<String> errors = new ArrayList<String>();
        for (Domain domain : currentUserDomains) {
            String hashedPassword = domain.getHashedPassword();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            byte[] hashedPasswordDigest = messageDigest.digest(hashedPassword.getBytes());
            String pair = domain.getDomainUrl() + new String(hashedPasswordDigest);

            String encryptedSignature = domain.getEncryptedSignature();
            Aes128Encryption aes128Encryption = new Aes128Encryption();
            String signatureSaved = aes128Encryption.decrypt(encryptedSignature, currentUser.getSkey());

            Signature signer = Signature.getInstance("SHA1WithRSA", new BouncyCastleProvider());
            signer.initSign((PrivateKey) keyStore.getKey("keyPrivate", Main.KEYSTORE_PASSWORD));
            signer.update(pair.getBytes());
            String signature = new String(signer.sign());

            if (!signature.equals(signatureSaved)) {
                errors.add("There is an unauthorized changed at domain with id: " + domain.getId() + " and domain url: " + domain.getDomainUrl());
            }
        }

        if (errors.size() > 0) {
            //https://stackoverflow.com/questions/523871/best-way-to-concatenate-list-of-string-objects
            throw new DomainPasswordUnauthorizedChangeException(String.join("\n", errors));
        }
    }
}
