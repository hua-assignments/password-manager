package com.passwordmanager;

import com.passwordmanager.models.User;

import com.passwordmanager.utils.SecurityUtils;
import com.passwordmanager.utils.Utils;
import com.passwordmanager.views.Home;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;

public class Main {

    public static char[] KEYSTORE_PASSWORD = "S0M#TH1NGV3RYS3CUR3".toCharArray();

    public static JFrame mainFrame;
    public static EntityManagerFactory entityManagerFactory;
    public static EntityManager entityManager;

    public static boolean isDbReady = false;

    public static User currentUser = null;

    public static KeyStore keyStore;

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static void main(String[] args) {
        createFrame();
        generateKeyStore();
        dbInitialize();
    }


    public static void dbInitialize() {
        entityManagerFactory = Persistence.createEntityManagerFactory("PM_JPA");
        entityManager = entityManagerFactory.createEntityManager();

        isDbReady = true;
    }

    public static void createFrame() {
        mainFrame = new JFrame("Password Manager");
        mainFrame.setSize(1000, 600);
        mainFrame.setContentPane(new Home().panelMain);
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.setVisible(true);


        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                exitApplication();
            }
        });
    }

    public static void exitApplication() {
        try {
            if(currentUser != null) {
                SecurityUtils.digitalSignUserDomains(currentUser, keyStore);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Before close application, close the db connection, dispose the main frame, logged out user
        if (isDbReady) {
            entityManager.close();
            entityManagerFactory.close();
        }
        mainFrame.dispose(); // Close Frame
        if (currentUser != null) { //  If user is logged in, logged him out and set null
            currentUser.setSkey(null);
            currentUser.setCertificate(null);
            currentUser = null;
        }
        Runtime.getRuntime().gc(); // Force java to call garbage collector to clean current user object and skey
        System.exit(0);
    }

    // Create CA Certificate and save into keystore if it does not exists
    private static void generateKeyStore() {
        try {
            File keystoreFile = new File("keystore/keystore.p12");
            if (!keystoreFile.exists()) {
                generate(keystoreFile);
            }

            keyStore = Utils.loadKeystore(keystoreFile, KEYSTORE_PASSWORD);
            X509Certificate certificate = (X509Certificate) keyStore.getCertificate("caCertificate");

            System.out.println("CA Certificate: " + certificate.toString());

            try {
                certificate.checkValidity(new Date());
            } catch (CertificateExpiredException ex) {
                // If certificate is expired generate new one
                ex.printStackTrace();
                keystoreFile.delete();
                generate(keystoreFile);
            } catch (CertificateNotYetValidException ex) {
                // If certificate is not yet valid throw the error
                ex.printStackTrace();
                exitApplication();
            }
        } catch (Exception e) {
            e.printStackTrace();
            exitApplication();
        }
    }

    // Generate application keypair, certificate and save into keystore
    private static void generate(File keystoreFile) throws NoSuchProviderException, NoSuchAlgorithmException,
            CertificateException, OperatorCreationException, IOException, KeyStoreException {
        File dir = new File("keystore");
        dir.mkdir();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
        keyPairGenerator.initialize(2048, new SecureRandom());
        KeyPair keyPair = keyPairGenerator.generateKeyPair(); // Create key pair with RSA 2048 Algorithm
        System.out.println("Private Key: " + keyPair.getPrivate().toString());

        X509Certificate caCertificate = SecurityUtils.createCaCertificate(keyPair, "cn=Myrtw Alex Bill");

        OutputStream keystoreOs = new FileOutputStream(keystoreFile);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        keyStore = SecurityUtils.generateKeystore(keyPair, caCertificate);

        // Store keystore to file
        keyStore.store(os, KEYSTORE_PASSWORD);
        keystoreOs.write(os.toByteArray());
        keystoreOs.close();
        os.close();
    }

}
