package com.passwordmanager.services;

import com.passwordmanager.models.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by bdrosatos on 22/11/2017.
 */
public class UsersService extends Service<User> {
    public UsersService(EntityManagerFactory entityManagerFactory, EntityManager entityManager) {
        super(entityManagerFactory, entityManager);
    }

    public User getById(long id) {
        return super.entityManager.find(User.class, id);
    }

    public List<User> getList(User criteria) {
        entityManager.getTransaction().begin();

//        List<User> users = entityManager.createQuery("SELECT U FROM User U").getResultList();
        TypedQuery<User> q = entityManager.createQuery("SELECT new User (U.id, U.username, U.email, U.firstName, U.lastName, U.hashedMasterPassword)" +
                " FROM User U " +
                " WHERE (?1 IS NULL OR U.username = ?1) AND  (?2 IS NULL OR U.email = ?2)", User.class);

        q.setParameter(1, criteria.getUsername() != null ? criteria.getUsername().toLowerCase() : null);
        q.setParameter(2, criteria.getEmail() != null ? criteria.getEmail().toLowerCase() : null);

        entityManager.getTransaction().commit();

        return q.getResultList();
    }
}
