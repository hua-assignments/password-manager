package com.passwordmanager.services;

import com.passwordmanager.models.Domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

public class DomainsService extends Service<Domain> {
    public DomainsService(EntityManagerFactory entityManagerFactory, EntityManager entityManager) {
        super(entityManagerFactory, entityManager);
    }

    public Domain getById(long id) {
        return super.entityManager.find(Domain.class, id);
    }

    public List<Domain> getList(Domain criteria) {
        entityManager.getTransaction().begin();

        TypedQuery<Domain> q = entityManager.createQuery("SELECT new Domain (D.id, D.domainUrl, D.username, D.hashedPassword, D.domainComment, D.encryptedSignature)" +
                " FROM Domain D INNER JOIN D.user U " +
                " WHERE (?1 IS NULL OR U.id = ?1)", Domain.class);

        q.setParameter(1, criteria.getUser() != null ? criteria.getUser().getId() : null);

        entityManager.getTransaction().commit();

        return q.getResultList();
    }

    public void remove(Domain domain) {
        Domain domainToBeRemoved = entityManager.getReference(Domain.class, domain.getId());
        entityManager.remove(domainToBeRemoved);
    }
}
