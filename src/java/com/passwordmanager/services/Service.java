package com.passwordmanager.services;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;


public abstract class Service<T> {

    protected EntityManagerFactory entityManagerFactory;
    protected EntityManager entityManager;

    public Service(EntityManagerFactory entityManagerFactory, EntityManager entityManager) {
        this.entityManagerFactory = entityManagerFactory;
        this.entityManager = entityManager;
    }

    // INSERT
    public void create(T object) {
        entityManager.getTransaction().begin();

        entityManager.persist(object);
        entityManager.getTransaction().commit();
    }

    // UPDATE
    public void update(T newObject) {

        entityManager.getTransaction().begin();
        entityManager.merge(newObject);
        entityManager.getTransaction().commit();
    }

    // DELETE
    public void remove(T object) {
        entityManager.getTransaction().begin();

        entityManager.remove(object);
        entityManager.getTransaction().commit();
    }

    // SELECT * BY ID
    public abstract T getById(long id);

    // SELECT * BY criteria
    public abstract List<T> getList(T criteria);
}
