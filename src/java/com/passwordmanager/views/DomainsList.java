package com.passwordmanager.views;

import com.passwordmanager.Main;
import com.passwordmanager.models.Domain;
import com.passwordmanager.services.DomainsService;
import com.passwordmanager.utils.Aes128Encryption;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class DomainsList {
    private JList<Domain> domainsJList;
    public JPanel panelDomainsList;
    private JButton addNewDomainButton;

    private DomainsService domainsService;

    public DomainsList() {
        this.domainsService = new DomainsService(Main.entityManagerFactory, Main.entityManager);

        showDomains();

        addNewDomainButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainFrame.setContentPane(new DomainAdd().panelDomainAdd);
                Main.mainFrame.revalidate();
                Main.mainFrame.repaint();

            }
        });
    }

    private void showDomains() {
        Domain criteria = new Domain();
        criteria.setUser(Main.currentUser);

        List<Domain> domains = this.domainsService.getList(criteria);

        DefaultListModel<Domain> listModel = new DefaultListModel<Domain>();

        for (Domain domain : domains) {
            listModel.addElement(domain);
        }

        domainsJList.setModel(listModel);


        domainsJList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                Domain domain = domainsJList.getSelectedValue();
                Main.mainFrame.setContentPane(new DomainDetails(domain).panelDomainDetails);
                Main.mainFrame.revalidate();
                Main.mainFrame.repaint();
            }
        });
    }
}