package com.passwordmanager.views;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.passwordmanager.Main;
import com.passwordmanager.utils.MasterPasswordHash;
import com.passwordmanager.models.User;
import com.passwordmanager.services.UsersService;
import com.passwordmanager.utils.SecurityUtils;
import com.passwordmanager.utils.Utils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.List;

public class Register {
    public JPanel panelRegister;
    private JTextField usernameField;
    private JTextField emailField;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JPasswordField passwordField;
    private JButton registerButton;
    private JButton backButton;
    UsersService usersService;

    public Register() {
        this.usersService = new UsersService(Main.entityManagerFactory, Main.entityManager);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainFrame.setContentPane(new Home().panelMain);
                Main.mainFrame.revalidate();
                Main.mainFrame.repaint();
            }
        });


        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean success = true;

                String username = usernameField.getText().toLowerCase();
                String password = new String(passwordField.getPassword());
                String email = emailField.getText().toLowerCase();

                if (username.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Username is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (password.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Password is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (!password.matches(SecurityUtils.MID_SECURITY_PATTERN)) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Password should have at least 6 characters, at least one digit and at least one lowercase character", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                User criteriaUsername = new User();
                criteriaUsername.setUsername(username);
                List<User> usersByUsername = usersService.getList(criteriaUsername);// Get users by username from db

                User criteriaEmail = new User();
                criteriaEmail.setEmail(email);
                List<User> usersByEmail = usersService.getList(criteriaEmail); // Get users by email from db

                if (usersByUsername.size() > 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "User with this username exists", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                } else if (usersByEmail.size() > 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "User with this email exists", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }


                if (success) {
                    User user = new User();
                    user.setUsername(username);
                    user.setEmail(email);
                    user.setFirstName(firstNameField.getText());
                    user.setLastName(lastNameField.getText());
                    user.setPassword(password);

                    MasterPasswordHash masterPasswordHash = new MasterPasswordHash();
                    masterPasswordHash.setUser(user);
                    String authHash = null;
                    try {
                        masterPasswordHash.generateSKey();
                        authHash = masterPasswordHash.generateAuthHash();// the encrypted master password
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    user.setHashedMasterPassword(authHash);
                    usersService.create(user); // Insert user in db

                    //Create certificate and key pair for this user
                    KeyPairGenerator keyPairGenerator;
                    try {
                        keyPairGenerator = KeyPairGenerator.getInstance("RSA", BouncyCastleProvider.PROVIDER_NAME);
                        keyPairGenerator.initialize(2048, new SecureRandom());
                        KeyPair keyPair = keyPairGenerator.generateKeyPair(); // Create key pair with RSA 2048 Algorithm for the new user

                        X509Certificate certificate = SecurityUtils.createClientCertificate(keyPair, Main.keyStore,
                                "cn=" + user.getFirstName() + " " + user.getLastName());

                        System.out.println("User Private key: " + keyPair.getPrivate().toString());
                        System.out.println("User certificate: " + certificate.toString());

                        Utils.saveCertificatePrivateToUserFolder(certificate, keyPair.getPrivate(), user.getUsername());

                        JOptionPane.showMessageDialog(Main.mainFrame, "You registered successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
                        Main.mainFrame.setContentPane(new Login().panelLogin);
                        Main.mainFrame.revalidate();
                        Main.mainFrame.repaint();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(Main.mainFrame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        ex.printStackTrace();
                    }
                }


            }
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panelRegister = new JPanel();
        panelRegister.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:d:grow", "center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        final JLabel label1 = new JLabel();
        label1.setText("Username");
        CellConstraints cc = new CellConstraints();
        panelRegister.add(label1, cc.xy(1, 1));
        usernameField = new JTextField();
        panelRegister.add(usernameField, cc.xy(3, 1, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label2 = new JLabel();
        label2.setText("Email");
        panelRegister.add(label2, cc.xy(1, 3));
        emailField = new JTextField();
        panelRegister.add(emailField, cc.xy(3, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label3 = new JLabel();
        label3.setText("First Name");
        panelRegister.add(label3, cc.xy(1, 5));
        firstNameField = new JTextField();
        panelRegister.add(firstNameField, cc.xy(3, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label4 = new JLabel();
        label4.setText("Last Name");
        panelRegister.add(label4, cc.xy(1, 7));
        lastNameField = new JTextField();
        panelRegister.add(lastNameField, cc.xy(3, 7, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label5 = new JLabel();
        label5.setText("Password");
        panelRegister.add(label5, cc.xy(1, 9));
        passwordField = new JPasswordField();
        panelRegister.add(passwordField, cc.xy(3, 9, CellConstraints.FILL, CellConstraints.DEFAULT));
        registerButton = new JButton();
        registerButton.setText("Register");
        panelRegister.add(registerButton, cc.xy(3, 11));
        backButton = new JButton();
        backButton.setText("Back");
        panelRegister.add(backButton, cc.xy(3, 13));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panelRegister;
    }
}
