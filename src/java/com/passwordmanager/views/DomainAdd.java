package com.passwordmanager.views;

import com.passwordmanager.Main;
import com.passwordmanager.models.Domain;
import com.passwordmanager.services.DomainsService;
import com.passwordmanager.services.UsersService;
import com.passwordmanager.utils.Aes128Encryption;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

public class DomainAdd {
    public JPanel panelDomainAdd;
    private JTextField urlField;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JTextArea commentText;
    private JButton addButton;
    private JButton backButton;
    DomainsService domainsService;

    public DomainAdd() {
        this.domainsService = new DomainsService(Main.entityManagerFactory, Main.entityManager);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainFrame.setContentPane(new DomainsList().panelDomainsList);
                Main.mainFrame.revalidate();
                Main.mainFrame.repaint();
            }
        });
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String url = urlField.getText();
                String username = usernameField.getText().toLowerCase();
                String password = new String(passwordField.getPassword());
                String comment = commentText.getText();

                boolean success = true;

                if (url.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Url is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (username.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Username is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (password.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Password is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (success) {
                    Domain domain = new Domain();
                    domain.setDomainUrl(url);
                    domain.setUsername(username);
                    domain.setDomainComment(comment);
                    Aes128Encryption aes128Encryption = null;
                    try {
                        aes128Encryption = new Aes128Encryption();
                        String hashedPassword = aes128Encryption.encrypt(password, Main.currentUser.getSkey());
                        domain.setHashedPassword(hashedPassword);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    domain.setUser(Main.currentUser);

                    domainsService.create(domain);

                    Main.mainFrame.setContentPane(new DomainsList().panelDomainsList);
                    Main.mainFrame.revalidate();
                    Main.mainFrame.repaint();


                }
            }
        });
    }
}
