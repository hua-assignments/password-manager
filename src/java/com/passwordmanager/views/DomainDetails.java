package com.passwordmanager.views;

import com.passwordmanager.Main;
import com.passwordmanager.models.Domain;
import com.passwordmanager.services.DomainsService;
import com.passwordmanager.utils.Aes128Encryption;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

/**
 * Created by bdrosatos on 2/12/2017.
 */
public class DomainDetails {
    public JPanel panelDomainDetails;
    private JTextArea domainCommentTextArea;
    private JPasswordField domainPasswordField;
    private JButton showPasswordButton;
    private JTextField domainUsernameTextField;
    private JTextField domainUrlTextField;
    private JTextField domainIdTextField;
    private JButton backToListButton;
    private JButton updateButton;
    private JButton deleteButton;
    private JButton saveButton;
    private JButton cancelButton;
    private DomainsService domainsService;

    public DomainDetails(final Domain domain) {
        this.domainsService = new DomainsService(Main.entityManagerFactory, Main.entityManager);
        this.saveButton.setVisible(false);
        this.cancelButton.setVisible(false);
        backToListButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                backToList();
            }
        });

        showPasswordButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showPassword();
            }
        });

        showDomainDetails(domain);
        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                domainsService.remove(domain);
                backToList();
            }
        });
        updateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveButton.setVisible(true);
                cancelButton.setVisible(true);
                updateButton.setVisible(false);
                deleteButton.setVisible(false);
                backToListButton.setVisible(false);
                showPasswordButton.setVisible(false);

                domainUrlTextField.setEditable(true);
                domainUsernameTextField.setEditable(true);
                domainPasswordField.setEditable(true);
                domainCommentTextArea.setEditable(true);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showDetailsView();

                showDomainDetails(domain);
            }
        });
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String url = domainUrlTextField.getText();
                String username = domainUsernameTextField.getText().toLowerCase();
                String password = new String(domainPasswordField.getPassword());
                String comment = domainCommentTextArea.getText();

                boolean success = true;

                if (url.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Url is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (username.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Username is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (password.length() == 0) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Password is empty", "Error", JOptionPane.ERROR_MESSAGE);
                    success = false;
                }

                if (success) {
                    domain.setDomainUrl(url);
                    domain.setUsername(username);
                    domain.setDomainComment(comment);
                    Aes128Encryption aes128Encryption = null;
                    try {
                        aes128Encryption = new Aes128Encryption();
                        String hashedPassword = aes128Encryption.encrypt(password, Main.currentUser.getSkey());
                        domain.setHashedPassword(hashedPassword);

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    domain.setUser(Main.currentUser);

                    domainsService.update(domain);

                    showDomainDetails(domain);
                    showDetailsView();

                }
            }
        });
    }

    private void showPassword() {
        Aes128Encryption aes128Encryption = null;
        try {
            aes128Encryption = new Aes128Encryption();
            String password = aes128Encryption.decrypt(new String(domainPasswordField.getPassword()), Main.currentUser.getSkey());

            JOptionPane.showMessageDialog(Main.mainFrame, password, "Password",
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(Main.mainFrame, "You password is corrupted", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    private void backToList() {
        Main.mainFrame.setContentPane(new DomainsList().panelDomainsList);
        Main.mainFrame.revalidate();
        Main.mainFrame.repaint();
    }

    private void showDomainDetails(Domain domain) {
        this.domainIdTextField.setText(Integer.toString(domain.getId().intValue()));
        this.domainUrlTextField.setText(domain.getDomainUrl());
        this.domainUsernameTextField.setText(domain.getUsername());
        this.domainPasswordField.setText(domain.getHashedPassword());
        this.domainCommentTextArea.setText(domain.getDomainComment());
    }

    public void showDetailsView() {
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
        updateButton.setVisible(true);
        deleteButton.setVisible(true);
        backToListButton.setVisible(true);
        showPasswordButton.setVisible(true);
        domainUrlTextField.setEditable(false);
        domainUsernameTextField.setEditable(false);
        domainPasswordField.setEditable(false);
        domainCommentTextArea.setEditable(false);
    }
}
