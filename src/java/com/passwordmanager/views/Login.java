package com.passwordmanager.views;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.passwordmanager.Main;
import com.passwordmanager.exceptions.DomainPasswordUnauthorizedChangeException;
import com.passwordmanager.utils.MasterPasswordHash;
import com.passwordmanager.models.User;
import com.passwordmanager.services.UsersService;
import com.passwordmanager.utils.SecurityUtils;
import com.passwordmanager.utils.Utils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Login {
    public JPanel panelLogin;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JFileChooser fileChooserCertificate;
    private JFileChooser fileChooserPrivateKey;
    private JButton loginButton;
    private JButton backButton;
    private JButton chooseCertificateButton;
    private JButton choosePrivateKeyButton;
    private final static int MAX_AUTHENTICATION_TRIES = 10;

    private long delayMs = 500;
    private int authenticationTries = 0;

    private UsersService usersService;

    public Login() {
        this.usersService = new UsersService(Main.entityManagerFactory, Main.entityManager);

        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainFrame.setContentPane(new Home().panelMain);
                Main.mainFrame.revalidate();
                Main.mainFrame.repaint();
            }
        });

        chooseCertificateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooserCertificate = new JFileChooser();
                fileChooserCertificate.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return (f.isDirectory() || f.getName().toLowerCase().endsWith(".csr") || f.getName().toLowerCase().endsWith(".pem"));
                    }

                    @Override
                    public String getDescription() {
                        return "Certificate Signing Request (*.csr) | Certificate (*.pem)";
                    }
                });
                fileChooserCertificate.showOpenDialog(panelLogin);
            }
        });

        choosePrivateKeyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooserPrivateKey = new JFileChooser();
                fileChooserPrivateKey.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return (f.isDirectory() || f.getName().toLowerCase().endsWith(".pem"));
                    }

                    @Override
                    public String getDescription() {
                        return "Private key (*.pem)";
                    }
                });
                fileChooserPrivateKey.showOpenDialog(panelLogin);
            }
        });

        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (authenticationTries < MAX_AUTHENTICATION_TRIES) {
                    // https://stackoverflow.com/questions/2258066/java-run-a-function-after-a-specific-number-of-seconds
                    new Timer().schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    login();
                                }
                            },
                            authenticationTries * delayMs // For each authentication try, wait 500ms before execute code
                    );
                } else {
                    JOptionPane.showMessageDialog(Main.mainFrame, "You exceed the maximum authentication tries. You are blocked.", "Error",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
    }

    private void login() {

        if (fileChooserCertificate == null || fileChooserCertificate.getSelectedFile() == null) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Add your certificate", "Certificate",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        if (fileChooserPrivateKey == null || fileChooserPrivateKey.getSelectedFile() == null) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Add your private key", "Private key",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        X509Certificate clientCertificate;
        PrivateKey clientPrivateKey;

        try {

            clientCertificate = Utils.
                    loadCertificateFromFile(fileChooserCertificate.getSelectedFile());

            clientPrivateKey = Utils.
                    loadPrivateKeyFromFile(fileChooserPrivateKey.getSelectedFile());

            SecurityUtils.validateCertificateWithPrivateKey(clientCertificate, clientPrivateKey);

            SecurityUtils.validateClientCertificate(Main.keyStore, clientCertificate);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(Main.mainFrame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            authenticationTries++;
            return;
        }

        System.out.println("User Certificate: " + clientCertificate.toString());

        // Certificate validation succeed, validate password
        String username = usernameField.getText().toLowerCase();
        String password = new String(passwordField.getPassword());

        if (username.length() == 0) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Username is empty", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (password.length() == 0) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Password is empty", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }


        User criteria = new User();
        criteria.setUsername(username);
        List<User> users = this.usersService.getList(criteria);
        // User with this username does not exists
        if (users.isEmpty()) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Authentication failed. Check username/password.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // If exists, validate password
        User user = users.get(0);
        MasterPasswordHash masterPasswordHash = new MasterPasswordHash();
        user.setPassword(password);
        masterPasswordHash.setUser(user);
        String skey = null;
        String authhash = "";
        try {
            skey = masterPasswordHash.generateSKey();
            authhash = masterPasswordHash.generateAuthHash();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!authhash.equals(user.getHashedMasterPassword())) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Authentication failed. Check username/password.", "Error", JOptionPane.ERROR_MESSAGE);
            authenticationTries++;
            return;
        }

        // Validation of certificate && password done, user is authenticated
        authenticationTries = 0; // Reset authentication tries
        user.setCertificate(clientCertificate); // Set the certificate for the current user
        user.setSkey(skey);
        Main.currentUser = user;

        try {
            SecurityUtils.verifySignUserDomain(user, Main.keyStore);
        } catch (DomainPasswordUnauthorizedChangeException ex) {
            JOptionPane.showMessageDialog(Main.mainFrame, ex.getMessage(), "Unauthorized change", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Main.mainFrame.setContentPane(new DomainsList().panelDomainsList);
        Main.mainFrame.revalidate();
        Main.mainFrame.repaint();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panelLogin = new JPanel();
        panelLogin.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:d:grow", "center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        final JLabel label1 = new JLabel();
        label1.setText("Username");
        CellConstraints cc = new CellConstraints();
        panelLogin.add(label1, cc.xy(1, 1));
        usernameField = new JTextField();
        panelLogin.add(usernameField, cc.xy(3, 1, CellConstraints.FILL, CellConstraints.DEFAULT));
        passwordField = new JPasswordField();
        panelLogin.add(passwordField, cc.xy(3, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        loginButton = new JButton();
        loginButton.setText("Login");
        panelLogin.add(loginButton, cc.xy(3, 5));
        final JLabel label2 = new JLabel();
        label2.setText("Password");
        panelLogin.add(label2, cc.xy(1, 3));
        backButton = new JButton();
        backButton.setText("Back");
        panelLogin.add(backButton, cc.xy(3, 7));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panelLogin;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
