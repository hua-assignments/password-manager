package com.passwordmanager.models;

import javax.persistence.*;

@Entity
@Table(name = "PM_DOMAINS", schema = "ITP17107")
public class Domain {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String domainUrl;
    private String username;
    private String hashedPassword;
    @Column(length = 800)
    private String encryptedSignature;
    private String domainComment;

    @ManyToOne(fetch=FetchType.LAZY)
    public User user;

    public Domain(String domainUrl, String username, String hashedPassword, String domainComment) {
        this.domainUrl = domainUrl;
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.domainComment = domainComment;
    }

    public Domain(Long id, String domainUrl, String username, String hashedPassword, String domainComment) {
        this(domainUrl, username, hashedPassword, domainComment);
        this.id = id;
    }

    public Domain(Long id, String domainUrl, String username, String hashedPassword, String domainComment, String encryptedSignature) {
        this(id, domainUrl, username, hashedPassword, domainComment);
        this.encryptedSignature = encryptedSignature;
    }


    public Domain() {
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getDomainComment() {
        return domainComment;
    }

    public void setDomainComment(String comment) {
        this.domainComment = comment;
    }

    public String getEncryptedSignature() {
        return encryptedSignature;
    }

    public void setEncryptedSignature(String encryptedSignature) {
        this.encryptedSignature = encryptedSignature;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String toString() {
        return String.format("Domain: %s", this.domainUrl);
    }
}
