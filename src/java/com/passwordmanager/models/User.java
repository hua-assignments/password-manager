package com.passwordmanager.models;


import javax.persistence.*;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.List;

@Entity
@Table(name = "PM_USERS", schema = "ITP17107")
public class User implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;
    private String email;
    private String firstName;
    private String lastName;

    @Transient
    private String password;

    private String hashedMasterPassword;

    @Transient // DO NOT CREATE COLUMN TO DB, ONLY IN MEMORY
    private String skey;

    @Transient
    private X509Certificate certificate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private List<Domain> domains;

    public User(long id, String username, String email, String firstName, String lastName, String hashedMasterPassword) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hashedMasterPassword = hashedMasterPassword;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHashedMasterPassword() {
        return hashedMasterPassword;
    }

    public void setHashedMasterPassword(String hashedMasterPassword) {
        this.hashedMasterPassword = hashedMasterPassword;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    public List<Domain> getDomains() {
        return domains;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", hashedMasterPassword='" + hashedMasterPassword + '\'' +
                '}';
    }
}